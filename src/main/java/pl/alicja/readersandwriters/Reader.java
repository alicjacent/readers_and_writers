package pl.alicja.readersandwriters;

/**
 * Reader thread.
 * Each reader reads defined number of times
 * Reader has to wait until all writers finish their tasks.
 */
public class Reader extends Thread {

    private static final int READING_TIMES = 3;
    private ReadingRoom rr;

    public Reader(ReadingRoom rr, String name) {
        super(name);
        this.rr = rr;
    }

    @Override
    public void run() {
        for (int i = 0; i < READING_TIMES; i++) {
            try {
                Thread.sleep((int)(Math.random()*1000)); //we want reader to make a break before starting reading again
                System.out.println(getName() + " wants to start to read!");
                rr.beginReading();
                System.out.println(getName() + " is reading now!");
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Thread.currentThread().interrupt();
                System.out.println("Thread " + getName() + " was interrupted.");
                throw new RuntimeException(ex);
            }
            System.out.println(getName() + " finished reading!");
            rr.endReading();
        }
    }

}

package pl.alicja.readersandwriters;

/**
 * Writer thread.
 * Each writer writes defined number of times
 * Writer has to wait until no other process is running.
 */
public class Writer extends Thread {
    
    private static final int WRITING_TIMES = 3;
    private ReadingRoom rr;

    public Writer(ReadingRoom rr, String name) {
        super(name);
        this.rr = rr;
    }
    
    public void run() {
        for (int i = 0; i < WRITING_TIMES; i++) {
            try {
                Thread.sleep((int)(Math.random()*1000)); //we want writer to make a break before starting writing again
                System.out.println(getName() + " wants to start to write!");
                rr.beginWriting();
                System.out.println(getName() + " is writing now!");
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Thread.currentThread().interrupt();
                System.out.println("Thread " + getName() + " was interrupted.");
                throw new RuntimeException(ex);
            }
            System.out.println(getName() + " finished writing!");
            rr.endWriting();
        }
    }
    
    
    
}

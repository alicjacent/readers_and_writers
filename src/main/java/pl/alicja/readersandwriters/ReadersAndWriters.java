package pl.alicja.readersandwriters;

/**
 * Main class.
 * Create specified number of threads 
 * that represents two types of processes
 */
public class ReadersAndWriters {
    
    private static final int NUMBER_OF_READERS = 10;
    private static final int NUMBER_OF_WRITERS = 2;

    public static void main(String[] args) {

        ReadingRoom rr = new ReadingRoom();

        for (int i = 0; i < NUMBER_OF_READERS; i++) {
            new Reader(rr, "Reader " + (i + 1)).start();
        }

        for (int i = 0; i < NUMBER_OF_WRITERS; i++) {
            new Writer(rr, "Writer " + (i + 1)).start();
        }
    }

}

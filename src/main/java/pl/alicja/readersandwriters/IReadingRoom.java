package pl.alicja.readersandwriters;

/**
 * define actions which occurs in the program
 */
public interface IReadingRoom {
    
    public void beginReading();
    public void endReading();
    public void beginWriting();
    public void endWriting();
    
}

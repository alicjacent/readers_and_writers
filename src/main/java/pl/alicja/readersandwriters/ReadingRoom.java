package pl.alicja.readersandwriters;

/**
 * Place in code where the main action is defined.
 * Representation of two processes (reading & writing)
 * which needs the same resources to work.
 */
public class ReadingRoom implements IReadingRoom {

    private int writers = 0;
    private int readers = 0;
    private int waitingWriters = 0;
    private boolean writersTurn = false;


    /**
     * While reader is reading
     * other readers can read.
     */
    @Override
    synchronized public void beginReading() {
        while (writers > 0 || (waitingWriters > 0 && writersTurn)) {
            try {
                wait();
            } catch (InterruptedException ex) {
                Thread.currentThread().interrupt();
                throw new RuntimeException(ex);
            }
        }
        writersTurn = true;
        readers++;
    }

    @Override
    synchronized public void endReading() {
        readers--;
        if (readers == 0) {
            notifyAll();
        }

    }

    /**
     * While writer is writing 
     * no other writer can write 
     * and no reader can read.
     */
    @Override
    synchronized public void beginWriting() {
        waitingWriters++;
        while (readers > 0 || writers > 0) {
            try {
                wait();
            } catch (InterruptedException ex) {
                Thread.currentThread().interrupt();
                throw new RuntimeException(ex);
            }
        }
        waitingWriters--;
        writers++;
    }

    @Override
    synchronized public void endWriting() {
        writersTurn = false;
        writers--;
        notifyAll();
    }

}
